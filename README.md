## walleye-user 11 RP1A.201005.004.A1 6934943 release-keys
- Manufacturer: google
- Platform: msm8998
- Codename: walleye
- Brand: google
- Flavor: walleye-user
- Release Version: 11
- Id: RP1A.201005.004.A1
- Incremental: 6934943
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/walleye/walleye:11/RP1A.201005.004.A1/6934943:user/release-keys
- OTA version: 
- Branch: walleye-user-11-RP1A.201005.004.A1-6934943-release-keys
- Repo: google_walleye_dump_5398


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
